<?php

class Header_text extends WP_Widget {

    /**
     * Sets up the widgets name etc
     * classname: naam van de klasse
     * description: omschrijving van de widget in wordpress
     * parent::__construct: zorgt dat functie met de klasse, widget en de argumenten wordt gedraait
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'my_widget',
            'description' => 'Telefoonnummer widget',
        );
        parent::__construct( 'my_widget', 'Header_text', $widget_ops );
    }

    /**
     * Outputs the content of the widget
     * $args before en after halen argumenten op die zijn gedeclareert bij het specifieke veld waar de widget in zit
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        echo $args['after_widget'];
    }

    /**
     * Outputs the options form on admin
     * 1e regel checkt of de variabele voor titel al een waarde heeft anders krijgt deze de standaard waarde
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Nieuwe omschrijving', 'text_domain' );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Beschrijving:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <?php
    }

    /**
     * Processing widget options on save
     * nieuwe waarde van title wordt aangepast
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}



