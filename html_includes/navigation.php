<div class="row">
    <div class="=col-sm-12 ">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"></button>
            </div>
            <?php
            wp_nav_menu( array(
                    'menu' => 'willem-menu',
                    'theme_location' => 'willem-menu',
                    'container' => 'div',
                    'menu_class' => 'nav navbar-nav navbar-left',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'main-nav'
                )
            );?>
        </nav>
    </div>
</div>