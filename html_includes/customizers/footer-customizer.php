<?php

function footer_customizer( $wp_customize ) {

    $adres =  array('Geadresseerde','Straat','Postcode en plaats');

    $wp_customize->add_section(
        'footer_adres',
        array(
            'title' => 'Footer-content',
            'description' => 'Hier kan je de footer gegevens aanpassen',
            'priority' => 35,
        )
    );

    foreach($adres as $gegeven) {
        $wp_customize->add_setting(
            'adres_gegevens_' . $gegeven,
            array(
                'default' => 'Vul in',
            )
        );

        $wp_customize->add_control(
            'adres_gegevens_' . $gegeven,
            array(
                'label' => __($gegeven, 'default'),
                'section' => 'footer_adres',
                'type' => 'text',
                'settings' => 'adres_gegevens_' . $gegeven
            )
        );
    }
    require('partials/footer-customizer-postadres.php');
    require('partials/footer-customizer-telefoon_fax.php');
    require('partials/footer-customizer-email.php');
}
add_action( 'customize_register', 'footer_customizer' );





