<?php

$postadres = array('Geadresseerde','Postbus','Postcode en plaats');

    foreach($postadres as $gegeven) {
        $wp_customize->add_setting(
            'postadres_gegevens_' . $gegeven,
            array(
                'default' => 'Vul in',
            )
        );

        $wp_customize->add_control(
            'postadres_gegevens_' . $gegeven,
            array(
                'label' => __($gegeven, 'default'),
                'section' => 'footer_adres',
                'type' => 'text',
                'settings' => 'postadres_gegevens_' . $gegeven
            )
        );
    }






