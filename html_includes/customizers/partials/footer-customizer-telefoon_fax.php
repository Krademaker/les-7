<?php

$telefoon_fax = array('Algemeen','Taxi diensten','Contractvervoer','Fax');

    foreach($telefoon_fax as $gegeven) {
        $wp_customize->add_setting(
            'telefoon_fax_gegevens_' . $gegeven,
            array(
                'default' => 'Vul in',
            )
        );

        $wp_customize->add_control(
            'telefoon_fax_gegevens_' . $gegeven,
            array(
                'label' => __($gegeven, 'default'),
                'section' => 'footer_adres',
                'type' => 'text',
                'settings' => 'telefoon_fax_gegevens_' . $gegeven
            )
        );
    }






