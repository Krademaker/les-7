<?php
function koen_setup() {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
}

add_action('after_setup_theme','koen_setup');


function koen_get_assets(){

    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_script('app', get_template_directory_uri() . '/dist/main.js', array('jquery'), false, true);

}

add_action('wp_enqueue_scripts','koen_get_assets');

function register_my_menus() { //roep de menus aan wordpress hier worden twee verschillende menus aan gemaakt
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'extra-menu' => __( 'Extra Menu' ),
            'willem-menu' => __( 'Willem menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );

//Get top ancestor

function ourWidgetsInit(){

    register_sidebar( array(
        'name' => 'Telefoon_veld',
        'id' => 'area1',
        'before_widget' => '<div class="widget_telefoon">',
        'after_widget' => '</div>',
        'before_title' => '<div class="widget_telefoon_text">',
        'after_title' => '</div>'
    ));
}
add_action('widgets_init','ourWidgetsInit');


require('html_includes/widgets/custom-widget-telefoon.php');
require('html_includes/customizers/footer-customizer.php');



add_action( 'widgets_init', function(){
    register_widget( 'Header_text' );
});






add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_yourprefix_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'test_metabox',
        'title'         => __( 'Test Metabox', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        'closed'     => true, // Keep the metabox closed by default
    ) );

    $cmb->add_field( array(
        'name' => 'Test File List',
        'desc' => '',
        'id'   => 'wiki_test_file_list',
        'type' => 'file_list',
        'text' => array(
            'add_upload_files_text' => 'Replacement', // default: "Add or Upload Files"
            'remove_image_text' => 'Replacement', // default: "Remove Image"
            'file_text' => 'Replacement', // default: "File:"
            'file_download_text' => 'Replacement', // default: "Download"
            'remove_text' => 'Replacement', // default: "Remove"
        ),
    ) );

    $cmb->add_field( array(
        'name'    => 'Test File',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'wiki_test_image',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
        ),
    ) );

    $cmb->add_field( array(
        'name' => __( 'Test Text area', 'cmb2' ),
        'desc' => __( 'field description (optional)', 'cmb2' ),
        'id'   => $prefix . 'textarea',
        'type' => 'textarea',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Test Text area 2', 'cmb2' ),
        'desc' => __( 'field description (optional)', 'cmb2' ),
        'id'   => $prefix . 'textarea2',
        'type' => 'textarea',
        'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
}

function output_text_and_image() {
    $image = wp_get_attachment_image( get_post_meta( get_the_ID(), 'wiki_test_image_id', 1 ), 'medium' );
    echo $image;
    $text = get_post_meta( get_the_ID(), '_yourprefix_textarea', 1 );
    echo $text;
}

function cmb2_output_file_list( $file_list_meta_key, $img_size = 'medium' ) {



    // Get the list of files
    $files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );


    // Loop through them and output an image
    foreach ( (array) $files as $attachment_id => $attachment_url ) {
        echo wp_get_attachment_image( $attachment_id, $img_size );
    }

}