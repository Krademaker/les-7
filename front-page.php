<?php
get_header();
if (have_posts()) :while (have_posts()) : the_post(); //the_post variabele waar de titel en de content inkomen
endwhile;
endif; ?>
    
    
        <div class="row main-article">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <p class="main-article-header"><?php echo "Personenvervoer" ?></p>
                </div>
        </div>
        <div class=" row main-article">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="col-lg-8 col-md-8">
                        <h5><?php echo "Kwaliteit voorop"; ?></h5>
                        <p><?php echo "Het verplaatsen van mensen is een grote verantwoordelijkheid die op alle fronten hoge eisen stelt. Mensen zijn immers het meest kostbare ‘goed’ dat je kunt vervoeren.
                                Veiligheid heeft dan ook onze uiterste prioriteit. Dat komt bij Willemsen de Koning al jaren tot uiting in de strenge eisen die wij stellen aan onze vervoersmiddelen en personeel."?>
                        </div>
                        <div class="main-article-image col-sm-12">
                                <p><?php if ( has_post_thumbnail() ) {the_post_thumbnail('medium',array('class' => 'main-article-image'));}the_content(); ?></p>
                        </div>
                        </p>

                </div>
        </div>
    
    
        <div class="row duo-article">
            <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                    <?php
                    cmb2_output_file_list( 'wiki_test_file_list', 'small' );
                    ?>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                    <?php output_text_and_image(); ?>
            </div>
        </div>
    
  
        <div class="row logo-slider">
                <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 ">
                        <h2 class="logo-slider-text"><?php echo "Klanten"?></h2>
                        <?php echo do_shortcode('[slick-carousel-slider centermode="true" dots="false" image_size="original" slidestoshow="7"]'); ?>
                </div>
        </div>
    


<?php get_footer();
