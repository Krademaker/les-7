'use strict';

var gulp = require('gulp');
var source = require('vinyl-source-stream');
var sass = require('gulp-sass');
var browserify = require('browserify');

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./'));
});

gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
    gulp.watch('./src/js/**/*.js', ['browserify']);
});

gulp.task('browserify', function() {
    var bundleStream = browserify('./src/js/main.js').bundle();

    bundleStream
        .pipe(source('main.js'))
        .pipe(gulp.dest('./dist'))
});
