    
        <div class="row footer-header">
                <div class="col-lg-12">
                        <p class="footer-text"><?php echo "Contact" ?></p>
                 </div>
        </div>

        <div class="row footer">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <ul>

                                <li class="bold"><?php echo "Adres:"?></li>
                               <?php
                                        $adres =  array('Geadresseerde','Straat','Postcode en plaats');
                                        foreach($adres as $gegeven){
                                                if(get_theme_mod('adres_gegevens_' . $gegeven)) : ?>
                                                        <li><?php echo get_theme_mod('adres_gegevens_' . $gegeven) ?></li>
                                                <?php endif;
                                        }
                               ?>
                        </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <ul>
                                <li class="bold"><?php echo "Postadres:"?></li>
                                <?php
                                $postadres = array('Geadresseerde','Postbus','Postcode en plaats');
                                foreach($postadres as $gegeven){
                                        if(get_theme_mod('postadres_gegevens_' . $gegeven)) : ?>
                                                <li><?php echo get_theme_mod('postadres_gegevens_' . $gegeven) ?></li>
                                        <?php endif;
                                }
                                ?>
                        </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <ul>
                                <li class="bold"><?php echo "Telefoon/Fax:"?></li>
                                <?php
                                $telefoon_fax = array('Algemeen','Taxi diensten','Contractvervoer','Fax');
                                foreach($telefoon_fax as $gegeven){
                                        if(get_theme_mod('telefoon_fax_gegevens_' . $gegeven)) : ?>
                                                <li><?php echo get_theme_mod('telefoon_fax_gegevens_' . $gegeven) ?></li>
                                        <?php endif;
                                }
                                ?>
                        </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                        <ul>
                                <li class="bold"><?php echo "E-mail:"?></li>
                                <li><a href="#"><?php echo get_theme_mod('email_gegevens_') ?></a></li>
                        </ul>
                </div>
        </div>
        </div>
    </div><!--container-margin-->
</div><!--container-->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<?php wp_footer(); ?>
</body>
</html>
        
     